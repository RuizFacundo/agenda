package modelo;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.Controlador;
import presentacion.vista.Vista;

public class Agenda {
	private PersonaDAO persona;
	private PaisDAO pais;
	private ProvinciaDAO provincia;
	private DomicilioDAO domicilio;
	private LocalidadDAO localidad;

	public Agenda(DAOAbstractFactory metodo_persistencia) {
		this.persona = metodo_persistencia.createPersonaDAO();
		this.pais = metodo_persistencia.createPaisDAO();
		this.domicilio = metodo_persistencia.createDomicilioDAO();
		this.provincia = metodo_persistencia.createProvinciaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
	}

	//CRUD Personas
	public void agregarPersona(PersonaDTO persona) {
		this.persona.insert(persona);
		this.domicilio.insert(persona.getDomicilio());
	}

	public void borrarPersona(PersonaDTO persona) {
		this.persona.delete(persona);
	}

	public List<PersonaDTO> obtenerPersonas() {
		return this.ensamblarPersonas();
	}

	public void editarPersona(PersonaDTO persona) {
		this.persona.update(persona);
		this.domicilio.update(persona.getDomicilio());
	}
	
	//CRUD Domicilios
	public DomicilioDTO obtenerDomicilio(Integer personId) {
		return this.domicilio.readAll(personId);
	}
		
	public void editarDomicilio(DomicilioDTO domicilio) {
		this.domicilio.update(domicilio);
	}
		
	public void agregarDomicilio(DomicilioDTO domicilio) {
		this.domicilio.insert(domicilio);
	}

	//R paises
	public List<PaisDTO> obtenerPaises() {
		return this.pais.readAll();
	}

	//CRD provincias
	public List<ProvinciaDTO> obtenerProvincias(Integer paisId) {
		return this.provincia.readAll(paisId);
	}
	
	public void borrarProvincia(ProvinciaDTO provincia) {
		this.provincia.delete(provincia);
	}
	
	public void agregarProvincia(ProvinciaDTO provincia) {
		this.provincia.insert(provincia);
	}
	
	//CRD Localidades 
	public List<LocalidadDTO> obtenerLocalidades(Integer provinciaId){
		return this.localidad.readAll(provinciaId);
	}
	
	public void borrarLocalidad(LocalidadDTO localidad) {
		this.localidad.delete(localidad);
	}
	
	public void agregarLocalidad(LocalidadDTO localidad) {
		this.localidad.insert(localidad);
	}

	//Vaidaciones
	public boolean datosOK(PersonaDTO persona) {
		return datosRequeridosOK(persona) && formatoOK(persona);
	}
	
	public boolean datosRequeridosOK(PersonaDTO persona) {
		return !(persona.getTelefono() == null || persona.getTelefono().isEmpty() || persona.getNombre() == null
				|| persona.getNombre().isEmpty() || persona.getEmail() == null || persona.getEmail().isEmpty());
	}
	
	public boolean formatoOK(PersonaDTO persona) {
		return Pattern.matches("[0-9]+", persona.getTelefono()) && 
				Pattern.matches("[A-Za-z0-9+_.-]+@(.+)$", persona.getEmail());
	}
	
	private List<PersonaDTO> ensamblarPersonas() {
		return this.persona.readAll().stream().map( persona -> {
			 persona.setDomicilio(this.obtenerDomicilio(persona.getIdPersona()));
			return persona;
		}).collect(Collectors.toList());
	}

}

