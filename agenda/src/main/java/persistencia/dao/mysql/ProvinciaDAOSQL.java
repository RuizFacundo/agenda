package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;

public class ProvinciaDAOSQL implements ProvinciaDAO {
	
	private static final String readAll = "SELECT * FROM provincias WHERE IdPais = ?";
	private static final String delete = "DELETE FROM provincias WHERE idProvincia = ?";
	private static final String insert = "INSERT INTO provincias(IdPais, Nombre) VALUES(?, ?)";
	
	
	public List<ProvinciaDTO> readAll(Integer paisId) {
		PreparedStatement statement;
		ResultSet resultSet; 
		ArrayList<ProvinciaDTO> provincias = new ArrayList<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			statement.setInt(1, paisId);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				provincias.add(this.obtenerProvinciaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}

	@Override
	public void insert(ProvinciaDTO provincia) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, provincia.getIdPais());
			statement.setString(2, provincia.getNombre());

			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void delete(ProvinciaDTO provincia) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(provincia.getIdProvincia()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	private ProvinciaDTO obtenerProvinciaDTO(ResultSet resultSet) throws SQLException{
		Integer idProvincia = resultSet.getInt("IdProvincia");
		Integer idPais = resultSet.getInt("IdPais");
		String nombre = resultSet.getString("Nombre");
		return new ProvinciaDTO(idProvincia, idPais, nombre);
	}
	
	
}
