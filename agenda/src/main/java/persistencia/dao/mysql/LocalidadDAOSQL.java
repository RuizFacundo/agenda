package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;

public class LocalidadDAOSQL implements LocalidadDAO {
	private static final String readAll = "SELECT * FROM localidades Where IdProvincia = ?";
	private static final String delete = "DELETE FROM localidades WHERE idLocalidad = ?";
	private static final String insert ="INSERT INTO localidades(IdProvincia, Nombre)  VALUES (?, ?)";
	
	
	@Override
	public void insert(LocalidadDTO localidad) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, localidad.getIdProvincia());
			statement.setString(2, localidad.getNombre());

			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	@Override
	public List<LocalidadDTO> readAll(Integer provinciaId) {
		PreparedStatement statement;
		ResultSet resultSet; 
		ArrayList<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			statement.setInt(1, provinciaId);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				localidades.add(this.obtenerLocalidadDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidades;
	}
	
	@Override
	public void delete(LocalidadDTO localidad) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(localidad.getIdLocalidad()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	private LocalidadDTO obtenerLocalidadDTO(ResultSet resultSet) throws SQLException{
		Integer idProvincia = resultSet.getInt("IdProvincia");
		Integer idLocalidad = resultSet.getInt("IdLocalidad");
		String nombre = resultSet.getString("Nombre");
		return new LocalidadDTO(idLocalidad, idProvincia, nombre);
	}
}
