package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.DomicilioDTO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, telefono, Email, Cumpleanios, TipoContacto, Tecnologia, Futbol) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT IdPersona, Nombre, Telefono, Email, Cumpleanios, TipoContacto, Tecnologia, futbol FROM personas";
	private static final String update = "UPDATE agenda.personas SET Nombre= ?, Telefono= ?, Email= ?, Cumpleanios= ?, TipoContacto= ?, Tecnologia= ?, Futbol= ? WHERE idpersona = ? ";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement personaStatement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try
		{
			personaStatement = conexion.prepareStatement(insert);
			personaStatement.setInt(1, persona.getIdPersona());
			personaStatement.setString(2, persona.getNombre());
			personaStatement.setString(3, persona.getTelefono());
			personaStatement.setString(4, persona.getEmail());
			personaStatement.setString(5,persona.getCumpleanios());
			personaStatement.setString(6, persona.getTipoContacto());
			personaStatement.setString(7, persona.getTecnologia());
			personaStatement.setString(8, persona.getFutbol());

			if(personaStatement.executeUpdate() > 0)
			{
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean delete(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean esDeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				esDeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return esDeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; 
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(obtenerPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	 public boolean update(PersonaDTO persona) {
		 	PreparedStatement personaStatement;
			Connection conexion = Conexion.getConexion().getSQLConexion();
			try
			{
				personaStatement = conexion.prepareStatement(update);
				
				personaStatement.setString(1, persona.getNombre());
				personaStatement.setString(2, persona.getTelefono());
				personaStatement.setString(3, persona.getEmail());
				personaStatement.setString(4, persona.getCumpleanios());
				personaStatement.setString(5, persona.getTipoContacto());
				personaStatement.setString(6, persona.getTecnologia());
				personaStatement.setString(7, persona.getFutbol());
				personaStatement.setInt(8, persona.getIdPersona());
				
				if(personaStatement.executeUpdate() > 0)
				{
					conexion.commit();
					return true;
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			return false;
	    }

	private PersonaDTO obtenerPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("IdPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String email = resultSet.getString("Email");
		String fecha = resultSet.getString("Cumpleanios");
		String tipoContacto = resultSet.getString("TipoContacto");
		String tecnologia = resultSet.getString("Tecnologia");
		String futbol = resultSet.getString("Futbol");

		return new PersonaDTO(id, nombre, tel, email, fecha, tipoContacto, null, tecnologia, futbol);
	}
}
