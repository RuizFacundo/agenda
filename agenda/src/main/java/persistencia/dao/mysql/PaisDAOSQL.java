package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.PaisDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PaisDAO;

public class PaisDAOSQL implements PaisDAO {
	
	private static final String readall = "SELECT * FROM paises";
	private static final String delete = "DELETE FROM personas WHERE idPais = ?";

	@Override
	public List<PaisDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; 
		ArrayList<PaisDTO> personas = new ArrayList<PaisDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(obtenerPaisDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	public boolean delete(PaisDTO pais)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(pais.getIdPais()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	private PaisDTO obtenerPaisDTO(ResultSet resultSet) throws SQLException
	{
		Integer idPais = resultSet.getInt("IdPais");
		String nombre = resultSet.getString("Nombre");
		return new PaisDTO(idPais, nombre);
	}

}
