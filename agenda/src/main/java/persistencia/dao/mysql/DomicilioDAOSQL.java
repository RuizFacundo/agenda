package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.DomicilioDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DomicilioDAO;


public class DomicilioDAOSQL implements DomicilioDAO {
	
	private static final String insert = "INSERT INTO domicilios(Calle, Altura, Piso, Depto, Pais, Provincia, Localidad, IdPersona) VALUES(?, ?, ?, ?, ?, ?, ?, LAST_INSERT_ID()); ";
	private static final String readAll = "SELECT IdDomicilio, Calle, Altura, Piso, Depto, Pais, Provincia, Localidad FROM domicilios WHERE IdPersona= ?";
	private static final String update = "UPDATE agenda.domicilios SET Calle= ?, Altura= ?, Piso= ?, Depto= ?, Pais= ?, Provincia= ?, Localidad= ? WHERE iddomicilio = ? ";

	@Override
	public boolean insert(DomicilioDTO domicilio) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setString(1, domicilio.getCalle());
			statement.setInt(2,domicilio.getAltura());
			statement.setInt(3, domicilio.getPiso());
			statement.setString(4, domicilio.getDepto());
			statement.setString(5, domicilio.getPais());
			statement.setString(6, domicilio.getProvincia());
			statement.setString(7, domicilio.getLocalidad());
			if(statement.executeUpdate() > 0) 
			{
				conexion.commit();
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public DomicilioDTO readAll(Integer personaId) {
		PreparedStatement statement;
		ResultSet resultSet;
		DomicilioDTO domicilio = null;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			statement.setInt(1, personaId);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				domicilio = obtenerDomicilioDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return domicilio;
	}

	@Override
	public boolean update(DomicilioDTO domicilio) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, domicilio.getCalle());
			statement.setInt(2, domicilio.getAltura());
			statement.setInt(3, domicilio.getPiso());
			statement.setString(4, domicilio.getDepto());
			statement.setString(5, domicilio.getLocalidad());
			statement.setString(6, domicilio.getPais());
			statement.setString(7, domicilio.getProvincia());
			statement.setInt(8, domicilio.getId());
			if(statement.executeUpdate() > 0) {
				conexion.commit();
				return true;
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return false;
	}
	
	private DomicilioDTO obtenerDomicilioDTO(ResultSet resultSet) throws SQLException{
		Integer idDomicilio = resultSet.getInt("IdDomicilio");
		String calle = resultSet.getString("Calle");
		Integer altura = resultSet.getInt("Altura");
		Integer piso = resultSet.getInt("Piso");
		String depto = resultSet.getString("Depto");
		String nombreLocalidad = resultSet.getString("Localidad");
		String pais = resultSet.getString("Pais");
		String provincia = resultSet.getString("Provincia");
		return new DomicilioDTO(idDomicilio, calle, altura, piso, depto, pais, provincia, nombreLocalidad);
	}

}
