package persistencia.dao.interfaz;

import java.util.List;

import dto.ProvinciaDTO;


public interface ProvinciaDAO {
	
	public void insert(ProvinciaDTO provincia);
	
	public List<ProvinciaDTO> readAll(Integer paisId);
	
	public void delete(ProvinciaDTO provincia);

}
