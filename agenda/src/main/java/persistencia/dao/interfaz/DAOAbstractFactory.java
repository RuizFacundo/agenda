package persistencia.dao.interfaz;


public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	public PaisDAO createPaisDAO();
	public DomicilioDAO createDomicilioDAO();
	public ProvinciaDAO createProvinciaDAO();
	public LocalidadDAO createLocalidadDAO();
}
