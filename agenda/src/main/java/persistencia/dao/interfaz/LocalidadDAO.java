package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadDAO {
	
	public void insert(LocalidadDTO localidad);
	
	public List<LocalidadDTO> readAll(Integer provinciaId);
	
	public void delete(LocalidadDTO localidad);
	
}
