package persistencia.dao.interfaz;

import java.util.List;

import dto.PaisDTO;
import dto.PersonaDTO;

public interface PaisDAO {
	
	public List<PaisDTO> readAll();

}
