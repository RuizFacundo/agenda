package dto;

public class DomicilioDTO {
	private Integer id;
	private String calle;
	private Integer altura;
	private Integer piso;
	private String depto;
	private String pais;
	private String provincia;
	private String localidad;
	
	public DomicilioDTO(Integer id, String calle, Integer altura, Integer piso, String depto, String pais, String provincia, String localidad) 
	{
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.depto = depto;
		this.pais = pais;
		this.provincia = provincia;
		this.localidad = localidad;
		this.id = id;
	}
	
	public DomicilioDTO(Integer id, String calle, Integer altura, String pais, String provincia, String localidad)
	{
		this.calle = calle;
		this.altura = altura;
		this.pais = pais;
		this.provincia = provincia;
		this.localidad = localidad;
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCalle() 
	{
		return calle;
	}
	
	public void setCalle(String calle) 
	{
		this.calle = calle;
	}
	
	public Integer getAltura() 
	{
		return altura;
	}
	
	public void setAltura(Integer altura) 
	{
		this.altura = altura;
	}
	
	public Integer getPiso() 
	{
		return piso;
	}
	
	public void setPiso(Integer piso) 
	{
		this.piso = piso;
	}
	
	public String getDepto() 
	{
		return depto;
	}
	
	public void setDepto(String depto) 
	{
		this.depto = depto;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
	
}
