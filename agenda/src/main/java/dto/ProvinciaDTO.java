package dto;

public class ProvinciaDTO {
	private Integer idProvincia;
	private Integer idPais;
	private String nombre;
	
	public ProvinciaDTO(Integer idProvincia, Integer idPais, String nombre) {
		super();
		this.idProvincia = idProvincia;
		this.idPais = idPais;
		this.nombre = nombre;
	}
	
	public ProvinciaDTO( Integer idPais, String nombre) {
		super();
		this.idPais = idPais;
		this.nombre = nombre;
	}

	public Integer getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}

	public Integer getIdPais() {
		return idPais;
	}

	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	 public String toString()
	 {
		 return nombre;
	 }
	
	
}