package dto;

public class LocalidadDTO {
	private Integer idLocalidad;
	private Integer idProvincia;
	private String nombre;
	
	public LocalidadDTO(Integer idLocalidad, Integer idProvincia, String nombre) {
		super();
		this.idLocalidad = idLocalidad;
		this.idProvincia = idProvincia;
		this.nombre = nombre;
	}
	
	public LocalidadDTO(Integer idProvincia, String nombre) {
		super();
		this.idProvincia = idProvincia;
		this.nombre = nombre;
	}

	public Integer getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Integer idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public Integer getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	 public String toString()
	 {
		 return nombre;
	 }
}
