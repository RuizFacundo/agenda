package dto;

import java.util.Date;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String email;
	private String cumpleanios;
	private String tipoContacto;
	private DomicilioDTO domicilio;
	private String tecnologia;
	private String futbol;

	public PersonaDTO(int idPersona, String nombre, String telefono, String email, String cumpleanios, String tipoContacto, DomicilioDTO domicilio, String tecnologia, String futbol)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.cumpleanios = cumpleanios;
		this.email = email;
		this.tipoContacto = tipoContacto;
		this.domicilio = domicilio;
		this.tecnologia = tecnologia;
		this.futbol = futbol;
	}
	
	public int getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCumpleanios() {
		return cumpleanios;
	}

	public void setCumpleanios(String cumpleanios) {
		this.cumpleanios = cumpleanios;
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto.isEmpty() ? "---" : tipoContacto;
		this.tipoContacto = tipoContacto;
	}

	public DomicilioDTO getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(DomicilioDTO domicilio) {
		this.domicilio = domicilio;
	}

	public String getTecnologia() {
		return tecnologia;
	}

	public String setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia.isEmpty() ? "---" : tecnologia;
		return this.tecnologia;
	}

	public String getFutbol() {
		return futbol;
	}

	public String setFutbol(String futbol) {
		return this.futbol = futbol;
	}
}
