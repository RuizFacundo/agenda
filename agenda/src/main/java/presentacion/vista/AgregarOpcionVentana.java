package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;

public class AgregarOpcionVentana extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtCampoNuevo;
	private JButton btnAgregar;
	private JLabel lblTitulo;
	private boolean esVentanaAgregarProvincia;
	private static AgregarOpcionVentana INSTANCE;
	
	public static AgregarOpcionVentana getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new AgregarOpcionVentana(); 	
			return INSTANCE;
		}
		else
			return INSTANCE;
	}

	public AgregarOpcionVentana() {
		initialize();
	}

	private void initialize() {
		
		setBounds(300, 300, 277, 154);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(37, 84, 191, 21);
		contentPane.add(btnAgregar);
		
		lblTitulo = new JLabel("");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(37, 10, 191, 13);
		contentPane.add(lblTitulo);
		
		txtCampoNuevo = new JTextField();
		txtCampoNuevo.setBounds(37, 55, 191, 19);
		contentPane.add(txtCampoNuevo);
		txtCampoNuevo.setColumns(10);
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}
	
	public void cerrarVentana() {
		this.setVisible(false);
		this.txtCampoNuevo.setText(null);
	}
	

	public JTextField getTxtCampoNuevo() {
		return txtCampoNuevo;
	}

	public void setTxtCampoNuevo(JTextField txtCampoNuevo) {
		this.txtCampoNuevo = txtCampoNuevo;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JLabel getLblTitulo() {
		return lblTitulo;
	}

	public void setLblTitulo(String lblTitulo) {
		this.lblTitulo.setText(lblTitulo);
	}

	public boolean isEsVentanaAgregarProvincia() {
		return esVentanaAgregarProvincia;
	}

	public void setEsVentanaAgregarProvincia(boolean esVentanaAgregarProvincia) {
		this.esVentanaAgregarProvincia = esVentanaAgregarProvincia;
	}
	
	
	
	
}
