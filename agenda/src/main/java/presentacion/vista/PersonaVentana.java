package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Label;
import com.toedter.calendar.JDateChooser;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PersonaVentana extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDepto;
	private JComboBox localidad;
	private JComboBox provincia;
	private JComboBox pais;
	private JComboBox tipoDeContacto;
	private JComboBox tecnologia;
	private JComboBox futbol;
	private JButton btnAgregar;
	private JButton btnResetear;
	private JButton agregarProvincia;
	private JButton agregarLocalidad;
	private JButton borrarLocalidad;
	private JButton borrarProvincia;
	private JDateChooser calendario;
	private JLabel lblAltura;
	private JLabel lblEquipoFutbol;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel;
	private static PersonaVentana INSTANCE;

	public static PersonaVentana getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new PersonaVentana(); 	
			return INSTANCE;
		}
		else
			return INSTANCE;
	}

	public PersonaVentana() {
		
		setBounds(100, 100, 631, 492);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre y apellido:");
		lblNombre.setBounds(37, 33, 110, 30);
		contentPane.add(lblNombre);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(37, 94, 110, 30);
		contentPane.add(lblTelefono);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(37, 149, 110, 30);
		contentPane.add(lblEmail);
		
		JLabel lblFechaDeCumpleaos = new JLabel("Fecha de cumpleaños:");
		lblFechaDeCumpleaos.setBounds(37, 206, 168, 30);
		contentPane.add(lblFechaDeCumpleaos);
		
		JLabel lblTipoContacto = new JLabel("Tipo de contacto:");
		lblTipoContacto.setBounds(37, 263, 168, 30);
		contentPane.add(lblTipoContacto);
		
		Label lblTituloDatos = new Label("Datos Personales");
		lblTituloDatos.setBounds(34, 10, 131, 21);
		contentPane.add(lblTituloDatos);
		
		Label lblTituloDomicilio = new Label("Domicilio");
		lblTituloDomicilio.setBounds(251, 10, 131, 21);
		contentPane.add(lblTituloDomicilio);
		
		JLabel lblCalle = new JLabel("Calle:");
		lblCalle.setBounds(251, 33, 110, 30);
		contentPane.add(lblCalle);
		
		JLabel lblPiso = new JLabel("Piso:");
		lblPiso.setBounds(304, 94, 43, 30);
		contentPane.add(lblPiso);
		
		JLabel lblDepto = new JLabel("Depto:");
		lblDepto.setBounds(359, 94, 43, 30);
		contentPane.add(lblDepto);
		
		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(251, 149, 110, 30);
		contentPane.add(lblPais);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(251, 206, 110, 30);
		contentPane.add(lblProvincia);
		
		JLabel lblLocalidad = new JLabel("Localidad:");
		lblLocalidad.setBounds(251, 263, 110, 30);
		contentPane.add(lblLocalidad);
		
		JLabel lblTecnologia = new JLabel("Tecnologia:");
		contentPane.add(lblTecnologia);
		
		JLabel lblFutbol = new JLabel("Futbol:");
		contentPane.add(lblFutbol);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(37, 65, 168, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(37, 122, 168, 19);
		contentPane.add(txtTelefono);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(37, 179, 168, 19);
		contentPane.add(txtEmail);
		
		txtCalle = new JTextField();
		txtCalle.setColumns(10);
		txtCalle.setBounds(251, 65, 168, 19);
		contentPane.add(txtCalle);
		
		txtAltura = new JTextField();
		txtAltura.setColumns(10);
		txtAltura.setBounds(251, 122, 43, 19);
		contentPane.add(txtAltura);
		
		txtPiso = new JTextField();
		txtPiso.setColumns(10);
		txtPiso.setBounds(304, 122, 43, 19);
		contentPane.add(txtPiso);
		
		txtDepto = new JTextField();
		txtDepto.setColumns(10);
		txtDepto.setBounds(357, 122, 62, 19);
		contentPane.add(txtDepto);
		
		localidad = new JComboBox();
		localidad.setBounds(251, 295, 168, 19);
		contentPane.add(localidad);
		
		pais = new JComboBox();
		pais.setBounds(251, 179, 168, 18);
		contentPane.add(pais);
		
		provincia = new JComboBox();
		provincia.setBounds(251, 234, 168, 18);
		contentPane.add(provincia);
		
		tipoDeContacto = new JComboBox();
		tipoDeContacto.setBounds(37, 295, 168, 19);
		contentPane.add(tipoDeContacto);
		
		tecnologia = new JComboBox();
		tecnologia.setBounds(251, 344, 168, 19);
		contentPane.add(tecnologia);
		
		futbol = new JComboBox();
		futbol.setBounds(37, 344, 168, 19);
		contentPane.add(futbol);
		
		btnResetear = new JButton("Borrar");
		btnResetear.setBounds(304, 411, 110, 21);
		contentPane.add(btnResetear);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(34, 411, 260, 21);
		contentPane.add(btnAgregar);
		
		calendario = new JDateChooser();
		calendario.setBounds(37, 234, 168, 20);
		contentPane.add(calendario);
		
		lblAltura = new JLabel("Altura:");
		lblAltura.setBounds(248, 102, 46, 14);
		contentPane.add(lblAltura);
		
		lblEquipoFutbol = new JLabel("Equipo de Futbol:");
		lblEquipoFutbol.setBounds(37, 325, 100, 14);
		contentPane.add(lblEquipoFutbol);
		
		lblNewLabel_3 = new JLabel("Tecnologia");
		lblNewLabel_3.setBounds(251, 325, 100, 14);
		contentPane.add(lblNewLabel_3);
		
		agregarProvincia = new JButton("Agregar");
		agregarProvincia.setBounds(429, 233, 82, 21);
		contentPane.add(agregarProvincia);
		
		agregarLocalidad = new JButton("Agregar");
		agregarLocalidad.setBounds(429, 294, 82, 21);
		contentPane.add(agregarLocalidad);
		
		borrarLocalidad = new JButton("Borrar");
		borrarLocalidad.setBounds(521, 294, 82, 21);
		contentPane.add(borrarLocalidad);
		
		borrarProvincia = new JButton("Borrar");
		borrarProvincia.setBounds(521, 233, 82, 21);
		contentPane.add(borrarProvincia);
		
		lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(PersonaVentana.class.getResource("/resources/degradado.jpg")));
		lblNewLabel.setBounds(0, 0, 617, 453);
		contentPane.add(lblNewLabel);
	}
	
	public void resetearInputs() {
		this.getTxtAltura().setText(null);
		this.getTxtCalle().setText(null);
		this.getTxtDepto().setText(null);
		this.getTxtEmail().setText(null);
		this.getTxtNombre().setText(null);
		this.getTxtPiso().setText(null);
		this.getTxtTelefono().setText(null);
		this.resetearSelects();
		this.getTecnologia().setSelectedIndex(0);
		this.getFutbol().setSelectedIndex(0);
		this.getTipoDeContacto().setSelectedIndex(0);
	}
	
	public void resetearSelects() {
		this.getPais().removeAllItems();
		this.getProvincia().removeAllItems();
		this.getLocalidad().removeAllItems();
	}
	
	public void mostrarAlertaCamposObligatorios() {
		mostrarDialog("Los Campos Nombre, Telefono, Email son obligatorios. \n El telefono debe ser numerico. \n El mail es incorrecto.");
	}
	
	public void mostrarDialog(String mensaje){
		JFrame f;
		f = new JFrame();
		JOptionPane.showMessageDialog(f, mensaje, "Alerta", JOptionPane.WARNING_MESSAGE);
	}
	
	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void cerrarVentana() {
		this.setVisible(false);
		this.dispose();
	}
	
	

	public JButton getAgregarProvincia() {
		return agregarProvincia;
	}

	public void setAgregarProvincia(JButton agregarProvincia) {
		this.agregarProvincia = agregarProvincia;
	}

	public JButton getAgregarLocalidad() {
		return agregarLocalidad;
	}

	public void setAgregarLocalidad(JButton agregarLocalidad) {
		this.agregarLocalidad = agregarLocalidad;
	}

	public JButton getBorrarLocalidad() {
		return borrarLocalidad;
	}

	public void setBorrarLocalidad(JButton borrarLocalidad) {
		this.borrarLocalidad = borrarLocalidad;
	}

	public JButton getBorrarProvincia() {
		return borrarProvincia;
	}

	public void setBorrarProvincia(JButton borrarProvincia) {
		this.borrarProvincia = borrarProvincia;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	
	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public void setTxtCalle(JTextField txtCalle) {
		this.txtCalle = txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public void setTxtAltura(JTextField txtAltura) {
		this.txtAltura = txtAltura;
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public void setTxtPiso(JTextField txtPiso) {
		this.txtPiso = txtPiso;
	}

	public JTextField getTxtDepto() {
		return txtDepto;
	}

	public void setTxtDepto(JTextField txtDepto) {
		this.txtDepto = txtDepto;
	}

	public JComboBox getLocalidad() {
		return localidad;
	}

	public void setLocalidad(JComboBox localidad) {
		this.localidad = localidad;
	}

	public JComboBox getProvincia() {
		return provincia;
	}

	public void setProvincia(JComboBox provincia) {
		this.provincia = provincia;
	}

	public JComboBox getPais() {
		return pais;
	}

	public void setPais(JComboBox pais) {
		this.pais = pais;
	}

	public JComboBox getTipoDeContacto() {
		return tipoDeContacto;
	}

	public void setTipoDeContacto(JComboBox tipoDeContacto) {
		this.tipoDeContacto = tipoDeContacto;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnResetear() {
		return btnResetear;
	}

	public void setBtnResetear(JButton btnResetear) {
		this.btnResetear = btnResetear;
	}

	public JDateChooser getcalendario() {
		return calendario;
	}

	public void setcalendario(JDateChooser calendario) {
		this.calendario = calendario;
	}

	public JComboBox getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(JComboBox tecnologia) {
		this.tecnologia = tecnologia;
	}

	public JComboBox getFutbol() {
		return futbol;
	}

	public void setFutbol(JComboBox futbol) {
		this.futbol = futbol;
	}
}


