package presentacion.vista;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.PersonaDTO;

import javax.swing.JButton;

import persistencia.conexion.Conexion;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Vista
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnEditar;
	private JButton btnfutbol;
	private JComboBox comboTecnologia;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"Nombre y apellido","Telefono", "Email","Cumpleaños","Tipo Contacto", "Calle", "Altura", "Piso","Depto","Localidad","Provincia","Pais", "Tecnologia", "Futbol"};

	public Vista() 
	{
		super();
		initialize();
	}

	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(200, 300, 1159, 355);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		
		JPanel panel = new JPanel();
		panel.setBounds(21, 0, 1200, 800);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 1100, 182);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(2).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(2).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(3).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(3).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(4).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(4).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(5).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(5).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(6).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(6).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(7).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(7).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(8).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(8).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(9).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(9).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(10).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(10).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(11).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(11).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(12).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(12).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(13).setPreferredWidth(200);
		tablaPersonas.getColumnModel().getColumn(13).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(273, 228, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(372, 228, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(466, 228, 89, 23);
		panel.add(btnBorrar);
		
		btnfutbol = new JButton("Equipos de futbol");
		btnfutbol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnfutbol.setBounds(565, 228, 150, 23);
		panel.add(btnfutbol);
		
		 comboTecnologia = new JComboBox();
		comboTecnologia.setBounds(723, 228, 152, 22);
		panel.add(comboTecnologia);
		
		JLabel lblNewLabel = new JLabel("Reporte Tecnologia");
		lblNewLabel.setBounds(725, 204, 150, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("src\\main\\java\\resources\\degradado.jpg"));
		lblNewLabel_1.setBounds(0, 0, 1121, 321);
		panel.add(lblNewLabel_1);
		
		JLabel lblFondoFrame = new JLabel("Imagen Fondo");
		lblFondoFrame.setIcon(new ImageIcon("src\\main\\java\\resources\\degradado.jpg"));
		lblFondoFrame.setBounds(-14, -31, 46, 792);
		frame.getContentPane().add(lblFondoFrame);
	}

	public void show()
	{
		/*
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		*/
		this.frame.setVisible(true);
	}
	
	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); 
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String email = p.getEmail();
			String cumpleanios = p.getCumpleanios();
			String tipoContacto = p.getTipoContacto();
			String calle = p.getDomicilio().getCalle();
			String altura = (p.getDomicilio().getAltura() == 0 ) ? "---" : p.getDomicilio().getAltura().toString();
			String piso = (p.getDomicilio().getPiso() == 0) ? "---" : p.getDomicilio().getPiso().toString();
			String depto = p.getDomicilio().getDepto();
			String localidad = p.getDomicilio().getLocalidad();
			String provincia = p.getDomicilio().getProvincia();
			String pais = p.getDomicilio().getPais();
			String tecnologia = p.getTecnologia();
			String futbol = p.getFutbol();
			
			Object[] fila = { nombre, tel, email, cumpleanios, tipoContacto, calle, altura, piso, depto, localidad, provincia, pais, tecnologia, futbol};
			this.getModelPersonas().addRow(fila);
		}
		
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}


	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	

	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public JButton getBtnbtnfutbol() {
		return btnfutbol;
	}

	public void setBtnbtnfutbol(JButton btnbtnfutbol) {
		this.btnfutbol = btnbtnfutbol;
	}

	public JComboBox getComboTecnologia() {
		return comboTecnologia;
	}

	public void setComboTecnologia(JComboBox string) {
		this.comboTecnologia = string;
	}
}
