package presentacion.controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JComboBox;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import presentacion.vista.Vista;

public class Interprete {

	public static <T> void llenarJbox(List<T> opciones, JComboBox combo) {
		combo.addItem("");
		for (T elemento : opciones) {
			combo.addItem(elemento);
		}
	}

	public static <T> void llenarJboxTipoContacto(List<T> opciones, JComboBox combo) {
		combo.removeAllItems();
		combo.addItem("");
		for (T elemento : opciones) {
			combo.addItem(elemento);
		}
	}

	public static void resetearCombo(JComboBox combo) {
		combo.removeAllItems();
	}

	public static void mostrarReporteTecnologia(Vista vista) throws SQLException, JRException {

		if (vista.getComboTecnologia().getSelectedItem().toString().equalsIgnoreCase("Orden Alfabetico")) {

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/agenda", "root", "root");
			String sql = "select *,count(tecnologia)/(select count(*) from agenda.personas)*100 from agenda.personas group by tecnologia order by tecnologia asc";
			JasperDesign jdesign = JRXmlLoader.load("reportes\\TecnoAsc.jrxml");
			JRDesignQuery update = new JRDesignQuery();
			update.setText(sql);
			jdesign.setQuery(update);
			JasperReport jreport = JasperCompileManager.compileReport(jdesign);
			JasperPrint print = JasperFillManager.fillReport(jreport, null, con);
			JasperViewer.viewReport(print, false);

		} if (vista.getComboTecnologia().getSelectedItem().toString().equalsIgnoreCase("Orden Invertido")) {

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/agenda", "root", "root");
			String sql = "select *,count(tecnologia)/(select count(*) from agenda.personas)*100 from agenda.personas group by tecnologia order by tecnologia desc";
			JasperDesign jdesign = JRXmlLoader.load("reportes\\TecnoDesc.jrxml");
			JRDesignQuery update = new JRDesignQuery();
			update.setText(sql);
			jdesign.setQuery(update);
			JasperReport jreport = JasperCompileManager.compileReport(jdesign);
			JasperPrint print = JasperFillManager.fillReport(jreport, null, con);
			JasperViewer.viewReport(print, false);
		}
	}

	public static void mostrarReporteEquipos() throws SQLException, JRException {
		// TODO Auto-generated method stub
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/agenda", "root", "root");
		String sql = "select * from agenda.personas order by futbol asc";
		JasperDesign jdesign = JRXmlLoader.load("reportes\\Deporte.jrxml");
		JRDesignQuery update = new JRDesignQuery();
		update.setText(sql);
		jdesign.setQuery(update);
		JasperReport jreport = JasperCompileManager.compileReport(jdesign);
		JasperPrint print = JasperFillManager.fillReport(jreport, null, con);
		JasperViewer.viewReport(print, false);

	}

}
