package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import Enumerations.TipoContacto;
import Enumerations.futbol;
import Enumerations.tecnologias;
import modelo.Agenda;
import net.sf.jasperreports.engine.JRException;
import presentacion.reportes.*;
import presentacion.vista.AgregarOpcionVentana;
import presentacion.vista.PersonaVentana;
import presentacion.vista.Vista;
import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;

public class Controlador implements ActionListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private PersonaVentana ventanaPersona;
	private AgregarOpcionVentana ventanaAgregarOpcion;
	private Agenda agenda;

	public Controlador(Vista vista, Agenda agenda) {
		this.vista = vista;
		this.agenda = agenda;
		this.vista.getBtnAgregar().addActionListener(a -> mostrarVentanaAgregarPersona(a));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnEditar().addActionListener(z -> intentarEditarPersona(z));
		this.ventanaPersona = PersonaVentana.getInstance();
		this.ventanaAgregarOpcion = AgregarOpcionVentana.getInstance();
		this.ventanaAgregarOpcion.getBtnAgregar().addActionListener(g -> agregarOpcion());
		this.ventanaPersona.getBtnAgregar().addActionListener(p -> intentarGuardarPersona(p));
		this.ventanaPersona.getBtnResetear().addActionListener(k -> resetearInputs());
		this.ventanaPersona.getPais().addActionListener(l -> intentarLlenarComboProvincia(l));
		this.ventanaPersona.getProvincia().addActionListener(g -> intentarLlenarComboLocalidad(g));
		this.ventanaPersona.getAgregarProvincia().addActionListener(k -> agregarProvincia());
		this.ventanaPersona.getAgregarLocalidad().addActionListener(p -> agregarLocalidad());
		this.ventanaPersona.getBorrarLocalidad().addActionListener(j -> eliminarLocalidad());
		this.ventanaPersona.getBorrarProvincia().addActionListener(t -> eliminarProvincia());
		this.vista.getBtnbtnfutbol().addActionListener(x -> {
			try {
				abrirReporteFutbol(x);
			} catch (SQLException | JRException e1) {
				e1.printStackTrace();
			}
		});
		this.vista.getComboTecnologia().addActionListener(l -> {
			try {
				abrirReporte(l);
			} catch (SQLException | JRException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void inicializar() {
		this.refrescarTabla();
		this.vista.show();
		this.llenarCombosReportes();
	}
	
	private void agregarProvincia() {
		Object item = this.ventanaPersona.getPais().getSelectedItem();
		if(item != null) {
			if (item.toString().equalsIgnoreCase("") == false) {
				this.ventanaAgregarOpcion.setLblTitulo("Agregar Provincia");
				this.ventanaAgregarOpcion.mostrarVentana();
				this.ventanaAgregarOpcion.setEsVentanaAgregarProvincia(true);
			}
		}
	}
	
	private void agregarLocalidad() {
		Object item = this.ventanaPersona.getProvincia().getSelectedItem();
		if(item != null) {
			if (item.toString().equalsIgnoreCase("") == false) {
				this.ventanaAgregarOpcion.setLblTitulo("Agregar Localidad");
				this.ventanaAgregarOpcion.mostrarVentana();
			}
		}
	}
	
	private void eliminarProvincia() {
		Object item = this.ventanaPersona.getProvincia().getSelectedItem();
		if(item != null) {
			if (item.toString().equalsIgnoreCase("") == false) {
				ProvinciaDTO provincia = (ProvinciaDTO) item;
				this.agenda.borrarProvincia(provincia);
				this.resetearSelects();
			}
		}
	}
	
	private void eliminarLocalidad() {
		Object item = this.ventanaPersona.getLocalidad().getSelectedItem();
		if(item != null) {
			if (item.toString().equalsIgnoreCase("") == false) {
				LocalidadDTO localdiad = (LocalidadDTO) item;
				this.agenda.borrarLocalidad(localdiad);
				this.resetearSelects();
			}
		}
	}
	
	private void agregarOpcion() {
		boolean campoVacio = this.ventanaAgregarOpcion.getTxtCampoNuevo().getText().isEmpty();
		if(this.ventanaAgregarOpcion.isEsVentanaAgregarProvincia() && !campoVacio) {
			ProvinciaDTO provincia = obtenerProvincia();
			this.agenda.agregarProvincia(provincia);
		} else if(!campoVacio){
			LocalidadDTO localidad = obtenerLocalidad();
			this.agenda.agregarLocalidad(localidad);
		}
		this.resetearSelects();
		this.ventanaAgregarOpcion.cerrarVentana();
	}

	private void abrirReporteFutbol(ActionEvent x) throws SQLException, JRException {
		Interprete.mostrarReporteEquipos();
	}

	private void abrirReporte(ActionEvent l) throws SQLException, JRException {
		Interprete.mostrarReporteTecnologia(this.vista);
	}
	
	private ProvinciaDTO obtenerProvincia() {
		Object item = this.ventanaPersona.getPais().getSelectedItem();
		Integer idPaisSeleccionado = ((PaisDTO) item).getIdPais();
		String nombreProvincia = this.ventanaAgregarOpcion.getTxtCampoNuevo().getText();
		return new ProvinciaDTO(idPaisSeleccionado, nombreProvincia);
	}
	
	private LocalidadDTO obtenerLocalidad() {
		Object item = this.ventanaPersona.getProvincia().getSelectedItem();
		Integer idProvinciaSeleccionada = ((ProvinciaDTO) item).getIdProvincia();
		String nombreLocalidad = this.ventanaAgregarOpcion.getTxtCampoNuevo().getText();
		return new LocalidadDTO(idProvinciaSeleccionada, nombreLocalidad);
	}
	

	public void llenarCombosReportes() {
		vista.getComboTecnologia().addItem("");
		vista.getComboTecnologia().addItem("Orden Alfabetico");
		vista.getComboTecnologia().addItem("Orden Invertido");
	}

	private void mostrarVentanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.mostrarVentana();
		Interprete.llenarJboxTipoContacto(Arrays.asList(TipoContacto.values()),this.ventanaPersona.getTipoDeContacto());
		Interprete.llenarJbox(this.agenda.obtenerPaises(), this.ventanaPersona.getPais());
		Interprete.llenarJbox(Arrays.asList(tecnologias.values()), this.ventanaPersona.getTecnologia());
		Interprete.llenarJbox(Arrays.asList(futbol.values()), this.ventanaPersona.getFutbol());
	}

	private void resetearInputs() {
		this.ventanaPersona.resetearInputs();
		Interprete.llenarJbox(this.agenda.obtenerPaises(), this.ventanaPersona.getPais());
	}
	
	private void resetearSelects() {
		this.ventanaPersona.resetearSelects();
		Interprete.llenarJbox(this.agenda.obtenerPaises(), this.ventanaPersona.getPais());
	}

	private void intentarLlenarComboProvincia(ActionEvent l) {
		Object item = this.ventanaPersona.getPais().getSelectedItem();
		if(item != null) {
			if (item.toString().equalsIgnoreCase("") == false) {
				Integer idPaisSeleccionado = ((PaisDTO) item).getIdPais();
				Interprete.resetearCombo(this.ventanaPersona.getProvincia());
				Interprete.llenarJbox(this.agenda.obtenerProvincias(idPaisSeleccionado), this.ventanaPersona.getProvincia());
			} else {
				Interprete.resetearCombo(this.ventanaPersona.getProvincia());
			}
		}
	}
	
	private void intentarLlenarComboLocalidad(ActionEvent g) {
		Object item = this.ventanaPersona.getProvincia().getSelectedItem();
		if(item != null) {
			if(item.toString().equalsIgnoreCase("") == false) {
				Integer idProvinciaSeleccionada = ((ProvinciaDTO)item).getIdProvincia();
				Interprete.resetearCombo(this.ventanaPersona.getLocalidad());
				Interprete.llenarJbox(this.agenda.obtenerLocalidades(idProvinciaSeleccionada), this.ventanaPersona.getLocalidad());
			} else {
				Interprete.resetearCombo(this.ventanaPersona.getLocalidad());
			}
		}
	}

	private void intentarGuardarPersona(ActionEvent p) {
		PersonaDTO persona = this.obtenerPersona();
		if (this.agenda.datosOK(persona)) {
			this.guardarPersona(persona);
		} else {
			this.ventanaPersona.mostrarAlertaCamposObligatorios();
		}
	}

	private void guardarPersona(PersonaDTO persona) {
		this.agenda.agregarPersona(persona);
		this.ventanaPersona.resetearInputs();
		this.refrescarTabla();
		this.ventanaPersona.cerrarVentana();
	}

	public PersonaDTO obtenerPersona() {
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String tel = ventanaPersona.getTxtTelefono().getText();
		String email = ventanaPersona.getTxtEmail().getText();
		String tipoContacto = ventanaPersona.getTipoDeContacto().getSelectedItem().toString();
		Date fechaNueva = ventanaPersona.getcalendario().getDate();
		SimpleDateFormat formateo = new SimpleDateFormat("dd/MM");
		String fecha = formateo.format(fechaNueva);
		String tecnologia = ventanaPersona.getTecnologia().getSelectedItem().toString();
		String futbol = ventanaPersona.getFutbol().getSelectedItem().toString();
		return new PersonaDTO(0, nombre, tel, email, fecha, tipoContacto, obtenerDomicilio(), tecnologia, futbol);
	}

	private DomicilioDTO obtenerDomicilio() {
		String calle = ventanaPersona.getTxtCalle().getText();
		String altura = ventanaPersona.getTxtAltura().getText();
		String depto = ventanaPersona.getTxtDepto().getText();
		String piso = ventanaPersona.getTxtPiso().getText();
		String pais = ventanaPersona.getPais().getSelectedIndex() == -1 ? ""
				: ventanaPersona.getPais().getSelectedItem().toString();
		String provincia = ventanaPersona.getProvincia().getSelectedIndex() == -1 ? ""
				: ventanaPersona.getProvincia().getSelectedItem().toString();
		String localidad = ventanaPersona.getLocalidad().getSelectedIndex() == -1 ? "" 
				: ventanaPersona.getLocalidad().getSelectedItem().toString();
		Integer parseAltura = Pattern.matches("[0-9]+", altura) ? Integer.parseInt(altura) : 0;
		Integer parsePiso = Pattern.matches("[0-9]+", piso) ? Integer.parseInt(piso) : 0;
		return new DomicilioDTO(0, calle, parseAltura, parsePiso, depto, pais, provincia, localidad);
	}

	private void mostrarReporte(ActionEvent r) {
		ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
		reporte.mostrar();
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
		}
		this.refrescarTabla();
	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.vista.llenarTabla(this.personasEnTabla);
	}

	public void intentarEditarPersona(ActionEvent b) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();

		for (int fila : filasSeleccionadas) {
			PersonaDTO persona = obtenerPersonaDeFila(fila);
			if (this.agenda.datosOK(persona)) {
				this.agenda.editarPersona(persona);
				this.refrescarTabla();
			} else {
				this.ventanaPersona.mostrarAlertaCamposObligatorios();
			}
		}
	}

	private PersonaDTO obtenerPersonaDeFila(int fila) {
		String nombre = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 0);
		String telefono = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 1);
		String email = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 2);
		String cumpleanios = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 3);
		String tipoContacto = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 4);
		String tecnologia = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 12);
		String futbol = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 13);

		String calle = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 5);
		String altura = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 6);
		String piso = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 7);
		String depto = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 8);
		String pais = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 9);
		String provincia = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 10);
		String localidad = (String) this.vista.getTablaPersonas().getModel().getValueAt(fila, 11);
		Integer idPersona = this.personasEnTabla.get(fila).getIdPersona();
		Integer idDomicilio = this.personasEnTabla.get(fila).getDomicilio().getId();
		Integer parseAltura = Pattern.matches("[0-9]+", altura) ? Integer.parseInt(altura) : 0;
		Integer parsePiso = Pattern.matches("[0-9]+", piso) ? Integer.parseInt(piso) : 0;
		DomicilioDTO domicilio = new DomicilioDTO(idDomicilio, calle, parseAltura, parsePiso, depto, pais, provincia,
				localidad);
		return new PersonaDTO(idPersona, nombre, telefono, email, cumpleanios, tipoContacto, domicilio, tecnologia,
				futbol);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}
